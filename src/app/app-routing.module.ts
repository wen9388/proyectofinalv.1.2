import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'main',
    children: [
      {
        path: "",
        loadChildren: "./main/main.module#MainPageModule",
      },
      {
        path: "add-residence",
        loadChildren: './main/add-residence/add-residence.module#AddResidencePageModule'
      },
      {
        path: "add-user",
        loadChildren: './main/add-user/add-user.module#AddUserPageModule'
      },
      {
        path: "edit-residence/:residenceId",
        loadChildren: './main/edit-residence/edit-residence.module#EditResidencePageModule'
      },
      {
        path: "view-lessee/",
        loadChildren: './main/view-lessee/view-lessee.module#ViewLesseePageModule'
      },
      {
        path: "detail-lessee/:residenceId",
        loadChildren: './main/detail-lessee/detail-lessee.module#DetailLesseePageModule'
      },
      {
        path: "add-reservation",
        loadChildren: './main/add-reservation/add-reservation.module#AddReservationPageModule'
      },
      {
        path: "detail-residence/:residenceId",
        loadChildren: './main/detail-residence/detail-residence.module#DetailResidencePageModule'
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
