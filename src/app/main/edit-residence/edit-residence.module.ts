import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditResidencePageRoutingModule } from './edit-residence-routing.module';

import { EditResidencePage } from './edit-residence.page';
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EditResidencePageRoutingModule
  ],
  declarations: [EditResidencePage]
})
export class EditResidencePageModule {}
