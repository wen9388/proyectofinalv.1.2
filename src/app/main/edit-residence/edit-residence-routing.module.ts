import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditResidencePage } from './edit-residence.page';

const routes: Routes = [
  {
    path: '',
    component: EditResidencePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditResidencePageRoutingModule {}
