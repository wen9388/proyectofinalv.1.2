import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { MainService } from "../main.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { residence } from '../main.model';

@Component({
  selector: 'app-edit-residence',
  templateUrl: './edit-residence.page.html',
  styleUrls: ['./edit-residence.page.scss'],
})
export class EditResidencePage implements OnInit {
  residence: residence;
  formEditResidence: FormGroup;
  constructor(
    private activeRouter: ActivatedRoute,
    private serviceResidence: MainService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activeRouter.paramMap.subscribe(paramMap => {
      if (!paramMap.has('residenceId')) {
        return;
      }
      const residenceId = parseInt(paramMap.get('residenceId'));
      this.residence = this.serviceResidence.getResidence(residenceId);
    });

    this.formEditResidence = new FormGroup({
      pcodeRes: new FormControl(this.residence.codeRes, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pnameRes: new FormControl(this.residence.nameRes, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      plocationRes: new FormControl(this.residence.locationRes, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      proomsRes: new FormControl(this.residence.roomsRes, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pbedsRes: new FormControl(this.residence.bedsRes, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pparLot: new FormControl(this.residence.parLot, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pwifi: new FormControl(this.residence.wifi, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ppriceDay: new FormControl(this.residence.priceDay, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pcontactRes: new FormControl(this.residence.contactRes, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pcontactMail: new FormControl(this.residence.contactMail, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });
    this.formEditResidence.value.pnameRes = this.residence.nameRes;
  }
  editResidence() {

    if (!this.formEditResidence.valid) {
      return;
    }

    this.serviceResidence.editResidence(
      this.formEditResidence.value.pcodeRes,
      this.formEditResidence.value.pnameRes,
      this.formEditResidence.value.plocationRes,
      this.formEditResidence.value.proomsRes,
      this.formEditResidence.value.pbedsRes,
      this.formEditResidence.value.pparLot,
      this.formEditResidence.value.pwifi,
      this.formEditResidence.value.ppriceDay,
      this.formEditResidence.value.pcontactRes,
      this.formEditResidence.value.pcontactMail,

    );

    this.formEditResidence.reset();
    
    this.router.navigate(['./main/view-lessee']);
    
  }
}