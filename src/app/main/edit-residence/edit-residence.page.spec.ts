import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditResidencePage } from './edit-residence.page';

describe('EditResidencePage', () => {
  let component: EditResidencePage;
  let fixture: ComponentFixture<EditResidencePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditResidencePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditResidencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
