import { Component, OnInit, Input } from '@angular/core';

import { MainService } from "../main.service";
import { residence, user } from "../main.model";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";


@Component({
  selector: 'app-view-lessee',
  templateUrl: './view-lessee.page.html',
  styleUrls: ['./view-lessee.page.scss'],
})
export class ViewLesseePage implements OnInit {
  @Input()
  nameRes: string;
  residence: residence[];

  constructor(
    private MainServices: MainService,
    private router: Router,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    console.log("Carga inicial");
    this.residence = this.MainServices.getAllResidence();
  }

  ionViewWillEnter() {
    console.log("Se obtuvo la lista");
    this.residence = this.MainServices.getAllResidence();
  }
  view(code: number) {
    this.router.navigate(["/main/detail-lessee/" + code]);
  }

  delete(code: number) {
    this.alertController
      .create({
        header: "Borrar Residencia",
        message: "Esta seguro que desea borrar esta residencia?",
        buttons: [
          {
            text: "No",
            role: "no",
          },
          {
            text: "Borrar",
            handler: () => {
              this.MainServices.deleteResidence(code);
              this.residence = this.MainServices.getAllResidence();
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }
  update(code: number) {
    this.router.navigate(["/main/edit-residence/" + code]);
  }

}
