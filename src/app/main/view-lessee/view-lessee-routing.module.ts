import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewLesseePage } from './view-lessee.page';

const routes: Routes = [
  {
    path: '',
    component: ViewLesseePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewLesseePageRoutingModule {}
