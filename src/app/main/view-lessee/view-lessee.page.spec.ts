import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewLesseePage } from './view-lessee.page';

describe('ViewLesseePage', () => {
  let component: ViewLesseePage;
  let fixture: ComponentFixture<ViewLesseePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLesseePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewLesseePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
