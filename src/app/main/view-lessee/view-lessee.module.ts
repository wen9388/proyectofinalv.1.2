import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewLesseePageRoutingModule } from './view-lessee-routing.module';

import { ViewLesseePage } from './view-lessee.page';
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    ViewLesseePageRoutingModule
  ],
  declarations: [ViewLesseePage]
})
export class ViewLesseePageModule {}
