import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailLesseePage } from './detail-lessee.page';

const routes: Routes = [
  {
    path: '',
    component: DetailLesseePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailLesseePageRoutingModule {}
