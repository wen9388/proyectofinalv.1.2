import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailLesseePageRoutingModule } from './detail-lessee-routing.module';

import { DetailLesseePage } from './detail-lessee.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailLesseePageRoutingModule
  ],
  declarations: [DetailLesseePage]
})
export class DetailLesseePageModule {}
