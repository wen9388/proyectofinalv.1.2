import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailLesseePage } from './detail-lessee.page';

describe('DetailLesseePage', () => {
  let component: DetailLesseePage;
  let fixture: ComponentFixture<DetailLesseePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailLesseePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailLesseePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
