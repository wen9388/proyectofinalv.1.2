import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, MinLengthValidator, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { MainService } from "../main.service";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {
  formAddUser: FormGroup;

  constructor(
    private serviceUser: MainService,
    private router: Router
  ) { }

  ngOnInit() {
    this.formAddUser = new FormGroup({
      pnameUser: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pemailUser: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ppassUser: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ptypeUser: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });
  }
  
  addUser(){
    if (!this.formAddUser.valid){
      return;
    }
    this.serviceUser.addUser(
    this.formAddUser.value.pcodeUser,
    this.formAddUser.value.pnameUser,
    this.formAddUser.value.pemailUser,
    this.formAddUser.value.ppassUser,
    this.formAddUser.value.ptypeUser,
    );
    this.formAddUser.reset();

    this.router.navigate(["/main"]);
  }

}
