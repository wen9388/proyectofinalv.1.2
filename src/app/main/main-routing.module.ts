import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainPage } from './main.page';

const routes: Routes = [
  {
    path: '',
    component: MainPage
  },
  {
    path: 'add-residence',
    loadChildren: () => import('./add-residence/add-residence.module').then( m => m.AddResidencePageModule)
  },
  {
    path: 'add-user',
    loadChildren: () => import('./add-user/add-user.module').then( m => m.AddUserPageModule)
  },
  {
    path: 'edit-residence',
    loadChildren: () => import('./edit-residence/edit-residence.module').then( m => m.EditResidencePageModule)
  },
  {
    path: 'view-lessee',
    loadChildren: () => import('./view-lessee/view-lessee.module').then( m => m.ViewLesseePageModule)
  },
  {
    path: 'detail-lessee',
    loadChildren: () => import('./detail-lessee/detail-lessee.module').then( m => m.DetailLesseePageModule)
  },
  {
    path: 'add-reservation',
    loadChildren: () => import('./add-reservation/add-reservation.module').then( m => m.AddReservationPageModule)
  },
  {
    path: 'detail-residence',
    loadChildren: () => import('./detail-residence/detail-residence.module').then( m => m.DetailResidencePageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainPageRoutingModule {}
