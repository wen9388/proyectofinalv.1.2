import { Injectable } from '@angular/core';
import { residence, user, reservations } from "./main.model";
import { ActivatedRoute, Router } from "@angular/router";


@Injectable({
  providedIn: 'root'
})
export class MainService {

  private user: user [] =[
    {
      codeUser: 456,
      nameUser: "Wendolyn Campos Mena",
      emailUser: "wen@gmail.com",
      passUser: "123456",
      typeUser: "arrendatario"
    },
  ];

  private reservations: reservations [] =[
    {
      codeRes: 123,
      codeReservation: 789,
      checkIn: "10/01/2021",
      checkOut: "15/01/2021",
      people: 5,
    },
  ];
  private residence: residence[] = [
    {
      reservations: this.reservations,
      codeRes: 123,
      nameRes: "Casa Heredia",
      locationRes: "Heredia",
      roomsRes: 4,
      bedsRes: 8,
      parLot: "Yes",
      wifi: "Yes",
      priceDay: 100,
      contactRes: "Erick Munoz",
      contactMail: "erick@mail.com",

    },
  ];

  constructor(
    private router: Router,
  ) { }

  
  getAllUser() {
    return [...this.user];
  }
  
  getUser(userId: number) {
    return {
      ...this.user.find((user) => {
        return user.codeUser === userId;
      }),
    };
  }

  getAllResidence() {
    return [...this.residence];
  }

  getResidence(residenceId: number) {
    return {
      ...this.residence.find((residence) => {
        return residence.codeRes === residenceId;
      }),
    };
  }

  deleteResidence(residenceId: number) {
    this.residence = this.residence.filter((residence) => {
      return residence.codeRes !== residenceId;
    });
  }

  getAllReservation() {
    return [...this.reservations];
  }

  getReservation(reservationId: number) {
    return {
      ...this.reservations.find((reservations) => {
        return reservations.codeReservation === reservationId;
      }),
    };
  }

  deleteReservation(reservationId: number) {
    this.reservations = this.reservations.filter((reservations) => {
      return reservations.codeReservation !== reservationId;
    });
  }


  addUser(
    pcodeUser: number,
    pnameUser: string,
    pemailUser: string,
    ppassUser: string,
    ptypeUser: string,
){
    const user: user = {
    codeUser: Math.round(Math.random() * (100 - 3) + 3),
    nameUser: pnameUser,
    emailUser : pemailUser,
    passUser: ppassUser,
    typeUser: ptypeUser, 
  };
  if (user.typeUser == "arrendatario"){
    this.router.navigate(["/main/view-lessee"]);
  }else{
    this.router.navigate(["/main"])
  }

  this.user.push(user);
}

  addResidence(
    pcodeRes: number,
    pnameRes: string,
    plocationRes: string,
    proomsRes: number,
    pbedsRes: number,
    pparLot: string,
    pwifi: string,
    ppriceDay: number,
    pcontactRes: string,
    pcontactMail: string,    
  ) {
    const residence: residence = {
      reservations: this.reservations,
      codeRes:  Math.round(Math.random() * (100 - 3) + 3),
      nameRes: pnameRes,
      locationRes: plocationRes,
      roomsRes: proomsRes,
      bedsRes: pbedsRes,
      parLot: pparLot,
      wifi: pwifi,
      priceDay: ppriceDay,
      contactRes: pcontactRes,
      contactMail: pcontactMail,
    }
    this.residence.push(residence);
  }

  editResidence(
    pcodeRes: number,
    pnameRes: string,
    plocationRes: string,
    proomsRes: number,
    pbedsRes: number,
    pparLot: string,
    pwifi: string,
    ppriceDay: number,
    pcontactRes: string,
    pcontactMail: string,     
  ) {
    let index = this.residence.map((x) => x.codeRes).indexOf(pcodeRes);

    this.residence[index].codeRes = this.residence.map((x) => x.codeRes).indexOf(pcodeRes);
    this.residence[index].nameRes = pnameRes;
    this.residence[index].locationRes = plocationRes;
    this.residence[index].roomsRes = proomsRes;
    this.residence[index].bedsRes = pbedsRes;
    this.residence[index].parLot = pparLot;
    this.residence[index].wifi = pwifi;
    this.residence[index].priceDay = ppriceDay;
    this.residence[index].contactRes = pcontactRes;
    this.residence[index].contactMail = pcontactMail;

    console.log(this.residence);
  }

  addReservation(
    pcodeRes: number,
    pcodeReservation: number,
    pcheckIn: string,
    pcheckOut: string,
    ppeople: number,
  ){
    const reservatios: reservations = {
      codeRes:  pcodeRes,
      codeReservation: Math.round(Math.random() * (100 - 3) + 3),
      checkIn: pcheckIn,
      checkOut: pcheckOut,
      people: ppeople,
    }
    let index = this.residence.map((x) => x.codeRes).indexOf(pcodeRes);
  }
}

