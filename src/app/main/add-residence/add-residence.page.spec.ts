import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddResidencePage } from './add-residence.page';

describe('AddResidencePage', () => {
  let component: AddResidencePage;
  let fixture: ComponentFixture<AddResidencePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddResidencePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddResidencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
