import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddResidencePage } from './add-residence.page';

const routes: Routes = [
  {
    path: '',
    component: AddResidencePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddResidencePageRoutingModule {}
