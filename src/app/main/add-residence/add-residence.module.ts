import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddResidencePageRoutingModule } from './add-residence-routing.module';

import { AddResidencePage } from './add-residence.page';
import { ReactiveFormsModule } from "@angular/forms";


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    AddResidencePageRoutingModule
  ],
  declarations: [AddResidencePage]
})
export class AddResidencePageModule {}
