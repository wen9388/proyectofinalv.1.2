import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailResidencePage } from './detail-residence.page';

describe('DetailResidencePage', () => {
  let component: DetailResidencePage;
  let fixture: ComponentFixture<DetailResidencePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailResidencePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailResidencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
