import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailResidencePageRoutingModule } from './detail-residence-routing.module';

import { DetailResidencePage } from './detail-residence.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailResidencePageRoutingModule
  ],
  declarations: [DetailResidencePage]
})
export class DetailResidencePageModule {}
