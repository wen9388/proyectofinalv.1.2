import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailResidencePage } from './detail-residence.page';

const routes: Routes = [
  {
    path: '',
    component: DetailResidencePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailResidencePageRoutingModule {}
