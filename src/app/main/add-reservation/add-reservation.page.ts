import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { MainService } from "../main.service";
import { residence, reservations } from '../main.model';


@Component({
  selector: 'app-add-reservation',
  templateUrl: './add-reservation.page.html',
  styleUrls: ['./add-reservation.page.scss'],
})
export class AddReservationPage implements OnInit {

  formAddReservations: FormGroup;
  idResidence: number;
  res: residence;

  constructor(
    private activeRouter: ActivatedRoute,
    private residenceService: MainService,
    private router: Router
  ) { }

  ngOnInit() {
        //validations inputs

        this.formAddReservations = new FormGroup({
          pcheckIn: new FormControl(null, {
            updateOn: "blur",
            validators: [Validators.required],
          }),
          pcheckOut: new FormControl(null, {
            updateOn: "blur",
            validators: [Validators.required],
          }),
          ppeople: new FormControl(null, {
            updateOn: "blur",
            validators: [Validators.required],
          }),   
        });  

        this.activeRouter.paramMap.subscribe((paramMap) => {
          if (!paramMap.has("residenceId")) {
            return;
          }
          const resId = parseInt(paramMap.get("residenceId"));
          this.res = this.residenceService.getResidence(resId);
          this.idResidence = resId;
        });
  }

  addReservation() {
    //add reservations to array

    if (!this.formAddReservations.valid) {
      return;
    }

    this.residenceService.addReservation(
      this.formAddReservations.value.pcodeRes = this.idResidence,
      this.formAddReservations.value.pcodeReservation,
      this.formAddReservations.value.pcheckIn,
      this.formAddReservations.value.pcheckOut,
      this.formAddReservations.value.ppeople,
    );

    this.formAddReservations.reset(); //clean form

    this.router.navigate(["/main"]);
  }

}
