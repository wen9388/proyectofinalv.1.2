export interface user{
    codeUser: number;
    nameUser: string;
    emailUser: string;
    passUser: string;
    typeUser: string;    
}

export interface residence{
    codeRes: number;
    nameRes: string;
    locationRes: string;
    roomsRes: number;
    bedsRes: number;
    parLot: string;
    wifi: string;
    priceDay: number;
    contactRes: string;
    contactMail: string;
    reservations: reservations[];
}

export interface reservations {
    codeRes: number,
    codeReservation: number,
    checkIn: string,
    checkOut: string,
    people: number,
}